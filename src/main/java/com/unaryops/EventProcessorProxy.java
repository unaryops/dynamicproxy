package com.unaryops;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class EventProcessorProxy implements InvocationHandler {

    private Object target;
    private boolean transactional = false;

    public EventProcessorProxy(Object target) {
        this.target = target;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;

        processAnnotations(method);

        if (transactional) {
            try {
                System.out.println("Starting transaction");
                // Start transaction here
                result = method.invoke(target, args);
                // Stop transaction here
                System.out.println("Closing transaction");
            } catch (Exception e) {
                // Roll back changes
                System.out.println("Rolling back");
            }
        } else {
            result = method.invoke(target, args);
        }
        return result;
    }

    private void processAnnotations(Method method) {
        Annotation[] declaredAnnotations = method.getDeclaredAnnotations();

        for (Annotation annotation : declaredAnnotations) {
            if (annotation.toString().equals("Transactional")) {
                transactional = true;
            }
        }
    }
}
